#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
yum -y install git

# hostname
hostnamectl set-hostname mycluster --static

# Create User
useradd -s /bin/bash -c "Admin" -m admin
echo "Passw0rd" | passwd --stdin admin
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
##

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

## Init K3S
curl -sfL https://get.k3s.io | sh -
# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##

## Ingress Controller deployment
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/baremetal/deploy.yaml
sed -i "s/NodePort/LoadBalancer/g" deploy.yaml
sed -i "/LoadBalancer/a\  externalIPs:\n    - $LBIP" deploy.yaml
kubectl apply -f deploy.yaml
##

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
EOF
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││               Welcome to Code & Cloud Event!              ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││            This is your K3S single-node cluster.          ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
##
